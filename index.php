<?php

require('animal.php');
require('Frog.php');
require('Ape.php');

$mamal = new Animal("Shaun");

echo "Name : $mamal->name <br> ";
echo "Legs : $mamal->legs <br>";
echo "Cold Blooded : $mamal->cold_blooded <br><br>";

$kodok = new Frog("buduk");

echo "Name : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Cold Blooded: $kodok->cold_blooded <br>";
echo "Jum : ";$kodok->jump(); 

$sungokong = new Ape("Kera Sakti");

echo " <br><br> Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "Cold Blooded: $sungokong->cold_blooded <br>";
echo "Yell :";$sungokong->yell();

?>